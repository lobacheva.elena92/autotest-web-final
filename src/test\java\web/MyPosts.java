package web;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.Verify;
import junit.framework.Assert;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class MyPosts extends AbstractTest{

    //переход на страницу About
    @Test
    void aboutClick() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        WebElement about = driver.findElement(By.xpath("//ul[@class=\"svelte-1rc85o5\"]/li[1]"));
        about.click();
        TimeUnit.SECONDS.sleep(1);
        Assertions.assertNotNull(driver.findElement(By.xpath("//h1[contains(text(),'About Page')]")));
    }

    //Переход на страницу Contact
    @Test
    void contactClick() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        WebElement contact = driver.findElement(By.xpath("//ul[@class=\"svelte-1rc85o5\"]/li[2]"));
        contact.click();
        Assertions.assertNotNull(driver.findElement(By.xpath("//h1[contains(text(),'Contact us!')]")));
    }

    //Переход на страницу 2
    @Test
    void page2() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        WebElement nextPage = driver.findElement(By.xpath("//a[contains(text(),'Next Page')]"));
        nextPage.click();
        TimeUnit.SECONDS.sleep(2);
        Assertions.assertNotNull(driver.findElement(By.xpath("//a[@href='/?page=1']")));
        Assertions.assertNotNull(driver.findElement(By.xpath("//a[@href='/?page=3']")));
    }

    //Проверка активности кнопки Previous Page на первой странице
@Test
    void disabledButtonPreviousPage() throws InterruptedException {
    TimeUnit.SECONDS.sleep(2);
    WebElement disabledPreviousPage = driver.findElement(By.cssSelector(".svelte-d01pfs.disabled"));
    Assertions.assertNotNull(driver.findElement(By.cssSelector(".svelte-d01pfs.disabled")));
    Assertions.assertNotNull(driver.findElement(By.xpath("//a[@href='/?page=2']")));

    }

    //Проверка открытия поста
    @Test
    void openPost() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        WebElement nextPage = driver.findElement(By.xpath("//a[contains(text(),'Next Page')]"));
        nextPage.click();
        TimeUnit.SECONDS.sleep(1);
        WebElement postKarlson = driver.findElement(By.cssSelector(".post:nth-child(1) > .svelte-127jg4t:nth-child(1)"));
        postKarlson.click();
        TimeUnit.SECONDS.sleep(1);
        Assertions.assertNotNull(driver.findElement(By.xpath("//h1[contains(text(),'Мульт3')]")));
    }

//Проверка перехода к постам чужих пользователей
    @Test
    void switchBoxNotMyPosts() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        WebElement switchBox = driver.findElement(By.xpath("//button[@id='SMUI-form-field-1']"));
        switchBox.click();
        TimeUnit.SECONDS.sleep(1);
        Assertions.assertNotNull(driver.findElement(By.xpath("//a[@href='/?page=2&owner=notMe']")));
    }

    //Переход на 4-ю страницу, проверка на активность кнопки Next Page
@Test
    void disabledButtonNextPage() throws InterruptedException {
    TimeUnit.SECONDS.sleep(2);
    WebElement page2 = driver.findElement(By.xpath("//a[@href='/?page=2']"));
    page2.click();
    //TimeUnit.SECONDS.sleep(1);
    WebElement page3 = driver.findElement(By.xpath("//a[@href='/?page=3']"));
    page3.click();
    //TimeUnit.SECONDS.sleep(1);
    WebElement page4 = driver.findElement(By.xpath("//a[@href='/?page=4']"));
    page4.click();
    TimeUnit.SECONDS.sleep(1);
    WebElement disabledNextPage = driver.findElement(By.cssSelector(".svelte-d01pfs.disabled"));
    Assertions.assertNotNull(driver.findElement(By.cssSelector(".svelte-d01pfs.disabled")));
    Assertions.assertNotNull(driver.findElement(By.xpath("//a[@href='/?page=3']")));
}

//Переход на страницу Главная
@Test
    void homeFromPost() throws InterruptedException {
    TimeUnit.SECONDS.sleep(2);
    WebElement knife = driver.findElement(By.xpath("//a[@href='/posts/14914']"));
    knife.click();
    TimeUnit.SECONDS.sleep(2);
    WebElement home = driver.findElement(By.cssSelector(".logo.svelte-1rc85o5"));
    home.click();
    Assertions.assertNotNull(driver.findElement(By.cssSelector(".svelte-d01pfs.disabled")));
    Assertions.assertNotNull(driver.findElement(By.xpath("//a[@href='/?page=2']")));
}

//Проверка отображения постов на странице
@Test
    void displayPosts(){
    WebElement previewPosts = driver.findElement(By.cssSelector(".posts.svelte-127jg4t"));
        Assertions.assertNotNull(driver.findElement(By.xpath("//div[@class=\"posts svelte-127jg4t\"]/a[1]")));
}

//Создание поста
@Test
    void createDelPost() throws InterruptedException {
    TimeUnit.SECONDS.sleep(2);
    WebElement create = driver.findElement(By.xpath("//button[@id='create-btn']"));
        create.click();
        Assertions.assertNotNull(driver.findElement(By.xpath("//h1[contains(text(),'Create Post')]")));
}
//Открыть страницу профиля пользователя
@Test
    void openMyProfile() throws InterruptedException {
    TimeUnit.SECONDS.sleep(2);
    WebElement contact = driver.findElement(By.xpath("//ul[@class=\"svelte-1rc85o5\"]/li[3]"));
    contact.click();
    TimeUnit.SECONDS.sleep(2);
    WebElement profile = driver.findElement(By.xpath("//ul[@class=\"mdc-deprecated-list\"]/li[1]"));
    profile.click();
    Assertions.assertNotNull(driver.findElement(By.xpath("//h1[contains(text(),'Profile Page')]")));
    Assertions.assertNotNull(driver.findElement(By.xpath("//div[.='Ivkin Artem']")));
}

//Переход на страницу входа в профиль
@Test
    void openPageLogin() throws InterruptedException {
    TimeUnit.SECONDS.sleep(2);
    WebElement contact = driver.findElement(By.xpath("//ul[@class=\"svelte-1rc85o5\"]/li[3]"));
    contact.click();
    TimeUnit.SECONDS.sleep(2);
    WebElement profile = driver.findElement(By.xpath("//ul[@class=\"mdc-deprecated-list\"]/li[2]"));
    profile.click();
    Assertions.assertNotNull(driver.findElement(By.xpath("//form[@id='login']")));
    Assertions.assertNotNull(driver.findElement(By.cssSelector(".svelte-1rc85o5.selected")));
}

//Выход из профиля пользователя
@Test
    void logout() throws InterruptedException {
    TimeUnit.SECONDS.sleep(2);
    WebElement contact = driver.findElement(By.xpath("//ul[@class=\"svelte-1rc85o5\"]/li[3]"));
    contact.click();
    TimeUnit.SECONDS.sleep(2);
    WebElement profile = driver.findElement(By.xpath("//ul[@class=\"mdc-deprecated-list\"]/li[2]"));
    profile.click();
    Assertions.assertNotNull(driver.findElement(By.xpath("//form[@id='login']")));
}
}
